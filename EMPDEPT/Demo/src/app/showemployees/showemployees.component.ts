import { Component, OnInit } from '@angular/core';
import { EmpService } from '../emp.service';


declare var jQuery: any;
@Component({
  selector: 'app-showemployees',
  templateUrl: './showemployees.component.html',
  styleUrls: ['./showemployees.component.css']
})
export class ShowemployeesComponent  implements OnInit{
  employees: any;
 employee:any
  countries: any;
  department: any;
constructor(private service: EmpService){
  this.employee = {
    "empId":"",
    "empName":"",
    "salary":"",
    "gender":"",
    "doj":"",
    "country":"",
    "emailId":"",
    "password":"",
    "department": {
      "deptId":""
    }
  }
  // this.Employee=[
  //   {
  //     "empId":101,
  //     "empName":"Ajay",
  //     "salary":'877858',
  //     "gender":"Male",
  //     "doj":"10-10-2023",
  //     "country": "India", 
  //     "emailId":"ajay@gmail.com",
  //     "password":"Ajay"
  //   },
  //   {
  //     "empId": 102,
  //     "empName": "Jane",
  //     "salary": 55000,
  //     "gender": "Female",
  //     "doj": "11-11-2019",
  //     "country": "Canada",
  //     "emailId": "jane.smith@gmail.com",
  //     "password": "jane"
  //   },
  //   {
  //     "empId": 103,
  //     "empName": "sai",
  //     "salary": 65000,
  //     "gender": "Male",
  //     "doj": "07-08-2020",
  //     "country": "USA",
  //     "emailId": "sai@gamil.com",
  //     "password": "sai"
  //   },
  //   {
  //     "empId": 104,
  //     "empName": "Surendra",
  //     "salary": 85000,
  //     "gender": "Male",
  //     "doj": "05-22-2021",
  //     "country": "India",
  //     "emailId": "surendra@gmail.com",
  //     "password": "surendra"
  //   }, {
  //     "empId": 105,
  //     "empName": "harshith",
  //     "salary": 98700,
  //     "gender": "Female",
  //     "doj": "11-09-2022",
  //     "country": "USA",
  //     "emailId": "harshith@gmail.com",
  //     "password": "goodha"
  //   }

  // ]

}
ngOnInit() {
  this.service.getAllEmployees().subscribe((data: any) => {
    this.employees = data;
    console.log(data);
  });
  this.service.getCountries().subscribe((data: any) => {
    this.countries = data;
  });
  this.service.getAllDepartments().subscribe((data: any) => {
    this.department = data;
  });
}
editEmp(emp: any) {
  console.log(emp);
  this.employee = emp;
  jQuery('#empModel').modal('show');
}
updateEmp() {
  this.service.updateEmployee(this.employee).subscribe((data: any) => { 
    console.log(data); 
  });
}
deleteEmp(emp: any) {
  this.service.deleteEmployee(emp.empId).subscribe((data: any) => {
    console.log(data);
  });

 
  const i = this.employees.findIndex((employee: any) => {
    return employee.empId == emp.empId;
  });
  this.employees.splice(i, 1);
}
}
