import { Component,OnInit} from '@angular/core';
import { EmpService } from '../emp.service';
//import { EmpService } from '../emp.service';
@Component({
  selector: 'app-registartion',
  templateUrl: './registartion.component.html',
  styleUrls: ['./registartion.component.css']
})
export class RegistartionComponent  implements OnInit{

employee:any;
 // service: any;
  countries: any;
  departments: any;

  constructor( private service: EmpService){
    this.employee={
      "empId":"",
      "empName":"",
      "salary":"",
      "gender":"",
      "doj":"",
      "country":"",
      "emailId":"",
      "password":"",
      "department":{
        "deptId":""
      }
    

    }

  }
  ngOnInit() {
    this.service.getCountries().subscribe((data: any) => {
      this.countries = data;
      console.log(data);
    });
    
    this.service.getAllDepartments().subscribe((data: any) => {
      this.departments = data;
      console.log(data);
    });
  }



  Register(RegisterForm:any){
    alert("Employee Registration Success");
    console.log(this.employee);
  }
  register() {
   
    console.log(this.employee);
    this.service.registerEmployee(this.employee).subscribe((data: any) => {
      console.log(data);
    });
  }

}
