import { Component, OnInit } from '@angular/core';
import { EmpService } from '../emp.service';
import { Router ,RouterModule} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  emailId: any;
  password: any;
  emp: any;
  employees: any;

  constructor(private service: EmpService, private router:Router) {
    // this.employees = [
    //   {
    //     "empId":101,
    //     "empName":"Ajay",
    //     "salary":'877858',
    //     "gender":"Male",
    //     "doj":"10-10-2023",
    //     "country": "India", 
    //     "emailId":"ajay@gmail.com",
    //     "password":"Ajay"
    //   },
    //   {
    //     "empId": 102,
    //     "empName": "Jane Smith",
    //     "salary": 55000,
    //     "gender": "Female",
    //     "doj": "11-11-2023",
    //     "country": "Canada",
    //     "emailId": "jane.smith@gmail.com",
    //     "password": "jane"
    //   },
    //   {
    //     "empId": 103,
    //     "empName": "sai",
    //     "salary": 65000,
    //     "gender": "Male",
    //     "doj": "07-08-2023",
    //     "country": "USA",
    //     "emailId": "sai@gamil.com",
    //     "password": "sai"
    //   },
    //   {
    //     "empId": 104,
    //     "empName": "Surendra",
    //     "salary": 85000,
    //     "gender": "Male",
    //     "doj": "22-12-2023",
    //     "country": "India",
    //     "emailId": "surendra@gmail.com",
    //     "password": "surendra"
    //   }, {
    //     "empId": 105,
    //     "empName": "harshith",
    //     "salary": 98700,
    //     "gender": "Female",
    //     "doj": "30-09-2023",
    //     "country": "USA",
    //     "emailId": "Harshith@gmail.com",
    //     "password": "goodha"
    //   }];
  }

  ngOnInit() {
  }
 async loginSubmit( loginForm:any){
  localStorage.setItem("emailId", loginForm.emailId);
  if (loginForm.EmailId =='HR' && loginForm.password =='HR') {
    this.service.setUserLogIn();
    this.router.navigate(['ShowEmployees']);
    
  }  else {
    await this.service.empLogin(loginForm).then((empData: any) => {
      this.emp = empData;
    });

    if (this.emp!= null) { 
      
      
      this.service.setUserLogIn();
      this.router.navigate(['Product']);
    } else {
      alert("Invalid Credentials");
    }
  }

}


  login() {
    if (this.emailId == "HR" && this.password == "HR") {
      alert("Login Success");
      this.service.setUserLogIn();
      this.router.navigate(['Showemployees']);
      
    } else {
      this.employees.forEach((employee: any) => {
         if(this.emailId == employee.emailId && this.password == employee.password) {
            this.emp=employee
         }
      });
      if(this.emp!=null){
        alert('Login Succses')
      } else{
        alert("Inavalid Credentials")
      }
    }
  }
}


