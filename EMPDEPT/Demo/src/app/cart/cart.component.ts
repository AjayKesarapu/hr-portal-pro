import { Component, OnInit } from '@angular/core';
import { EmpService } from '../emp.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {


  cartItems: any;
  temp: any;
  total: number;

  constructor( private service:EmpService) {
    this.total = 0;
    this.cartItems = this.service.cartItems;

   
    this.temp = localStorage.getItem("cartProds");
    this.cartItems = JSON.parse(this.temp);

    console.log(this.cartItems);

    if (this.cartItems != null) {
      this.cartItems.forEach((product: any) => {
        this.total = this.total + product.price;
      });
    }

  }

  ngOnInit() {
  }

  deleteFromCart(prod: any) {
    const i = this.cartItems.findIndex((product: any) => {
      return product.id == prod.id;
    });

    this.total = this.total - this.cartItems[i].price;
    this.cartItems.splice(i, 1);    
  }

  checkOut() {
    this.total = 0;
    this.cartItems = null;    
  }

}