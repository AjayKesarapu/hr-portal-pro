import { Component, OnInit } from '@angular/core';
import { EmpService } from '../emp.service';

@Component({
  selector: 'app-show-emp-by-id',
  templateUrl: './show-emp-by-id.component.html',
  styleUrls: ['./show-emp-by-id.component.css']
})
export class ShowEmpByIdComponent implements OnInit {

empId:any
employee:any
  //Employee:any
  emailId: any;

constructor( private service: EmpService){
  this.emailId = localStorage.getItem("emailId");
//   this.Employee=[
//      {
//     "empId":101,
//     "empName":"Ajay",
//     "salary":'877858',
//     "gender":"Male",
//     "doj":"10-10-2023",
//     "country": "India", 
//     "emailId":"ajay@gmail.com",
//     "password":"Ajay"
//   },
//   {
//     "empId": 102,
//     "empName": "Jane",
//     "salary": 55000,
//     "gender": "Female",
//     "doj": "11-11-2023",
//     "country": "Canada",
//     "emailId": "jane.smith@gmail.com",
//     "password": "jane"
//   },
//   {
//     "empId": 103,
//     "empName": "sai",
//     "salary": 65000,
//     "gender": "Male",
//     "doj": "07-08-2023",
//     "country": "USA",
//     "emailId": "sai@gamil.com",
//     "password": "sai"
//   },
//   {
//     "empId": 104,
//     "empName": "Surendra",
//     "salary": 85000,
//     "gender": "Male",
//     "doj": "22-12-2023",
//     "country": "India",
//     "emailId": "surendra@gmail.com",
//     "password": "surendra"
//   }, {
//     "empId": 105,
//     "empName": "harshith",
//     "salary": 98700,
//     "gender": "Female",
//     "doj": "30-09-2023",
//     "country": "USA",
//     "emailId": "Harshith@gmail.com",
//     "password": "goodha"
//   }

// ]


}

  ngOnInit() {
  
  }
  getEmployee() {

    this.employee = null;

    this.service.getEmpById(this.empId).subscribe((data: any) => {
      this.employee = data;
      console.log(data);
    });
  }

}
