import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegistartionComponent } from './registartion/registartion.component';
import { ShowemployeesComponent } from './showemployees/showemployees.component';
import { LogoutComponent } from './logout/logout.component';
import { ShowEmpByIdComponent } from './show-emp-by-id/show-emp-by-id.component';
import { authGuard } from './auth.guard';
import { ProductsComponent } from './products/products.component';
import { CartComponent } from './cart/cart.component';



const routes: Routes = [ 
  {path:"",            component:LoginComponent},
  {path:"Login",       component:LoginComponent},
  {path:"Register",    component:RegistartionComponent},
  {path:"ShowEmployees", canActivate:[authGuard], component:ShowemployeesComponent},
  {path:"ShowEmployeeById", canActivate:[authGuard],component:ShowEmpByIdComponent},
  {path:"Logout",      canActivate:[authGuard],component:LogoutComponent},
  {path:"Product",     canActivate:[authGuard], component:ProductsComponent},
  {path:"cart",     canActivate:[authGuard], component:CartComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
