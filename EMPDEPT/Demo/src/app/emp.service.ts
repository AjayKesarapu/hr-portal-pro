import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class EmpService {
  
  loginStatus: Subject<any>;
  isUserLogged: boolean;
  cartItems: any;
  

  constructor(private http: HttpClient) { 
    this.isUserLogged = false;
    this.loginStatus = new Subject();
    this.cartItems = [];
  }

  addToCart(product: any) {
    this.cartItems.push(product);
  }
  // setUserLogIn() {
  //   this.isUserLogged = true;
  // }
  
  // setUserLogOut() {
  //   this.isUserLogged = false;
  // }
  // getLoginStatus(): boolean {
  //   return this.isUserLogged;
  // }
  
  getCountries(): any {
    return this.http.get('https://restcountries.com/v3.1/all');
  }

  getAllEmployees(): any {
    return this.http.get("http://localhost:8085/getAllEmployees");
  }

  
  getEmpById(empId: any): any {
    return this.http.get('http://localhost:8085/getEmpById/' + empId);
  }

  getAllDepartments(): any {
    return this.http.get('http://localhost:8085/getAllDepartments');
  }
  registerEmployee(emp: any): any {
    return this.http.post('registerEmployee', emp);
  }
  deleteEmployee(empId: any): any {
    return this.http.delete('deleteEmpById/' + empId);
  }
  updateEmployee(emp: any): any {
    return this.http.put('updateEmployee', emp);
  }
  getAllProducts(): any {
    return this.http.get("http://localhost:8085/getAllProducts");
  }
  empLogin(loginForm: any) {
    return this.http.get("empLogin/" + loginForm.emailId + "/" + loginForm.password).toPromise();
  }
  setUserLogIn() {
    this.isUserLogged = true;

    //To Enable and Disable Login, Register and Logout
    this.loginStatus.next(true);
  }
  //Logout
  setUserLogOut() {
    this.isUserLogged = false;

    //To Enable and Disable Login, Register and Logout
    this.loginStatus.next(false);
  }
  //AuthGuard
  getLoginStatus(): boolean {
    return this.isUserLogged;
  }

  //To Enable and Disable Login, Register and Logout
  getStatusLogin(): any {
    return this.loginStatus.asObservable();
  }

}



