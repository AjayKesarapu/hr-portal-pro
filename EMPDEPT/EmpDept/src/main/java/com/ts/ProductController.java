package com.ts;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dao.ProductDAO;
import com.model.Product;

@RestController
public class ProductController {
	@Autowired
ProductDAO prodDAO;
	
	@RequestMapping("getProductByName/{name}")
	public Product getProductByName(@PathVariable("name") String name){
		return prodDAO.getProductByName(name);
		
	}
@RequestMapping("showProduct/{id}")
public Product showProduct(@PathVariable("id") int Id){
	return prodDAO.getProductbyid(Id);
}

@RequestMapping("getAllProducts")
public  List<Product> getAllproducts(){
	return prodDAO.getAllProducts();
	}

@PostMapping("registerProduct")
public Product registerProduct(@RequestBody Product prod) {
	return prodDAO.registerProduct(prod);
}

}
