package com.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Product {
@Id
@GeneratedValue
	private  int Id;
	private String name;
	private String  description;
	private double price;
	private String imgsrc;
	public Product() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Product(int id, String name, String description, double price,String imgsrc) {
		super();
		Id = id;
		this.name = name;
		this.description = description;
		this.imgsrc = imgsrc;
		this.price=price;
	}
	public int getId() {
		return Id;
	}
	public void setId(int id) {
		Id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	
	
	
	public void setDescription(String description) {
		this.description = description;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public String getImgsrc() {
		return imgsrc;
	}
	public void setImgsrc(String imgsrc) {
		this.imgsrc = imgsrc;
	}
	@Override
	public String toString() {
		return "Product [Id=" + Id + ", name=" + name + ", descrption=" + description + ", price=" + price +", imgsrc=" + imgsrc + "]";
	}
	

}
