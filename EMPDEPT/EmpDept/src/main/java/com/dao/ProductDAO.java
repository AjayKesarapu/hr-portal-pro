package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.Product;

@Service
public class ProductDAO {
	@Autowired
	ProductRepository prodRepo;
	public List<Product> getAllProducts(){
		return prodRepo.findAll();
	}
public  Product getProductbyid(int id){
	
	return prodRepo.findById(id).orElse(null);
}
public Product getProductByName(String name){
	return  prodRepo.findByName(name);
	
	
}
public Product registerProduct(Product prod){
	return prodRepo.save(prod);
	
}


}
